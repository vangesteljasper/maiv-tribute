# Consult 01
---


## Voor het gesprek

- design: wij dachten eraan om vooral veel wireframes en flowcharts te maken, aangezien we heel veel longreads zijn tegengekomen waar er weinig design, maar vooral veel interactiviteit aanwezig is (video’s of veranderende foto’s als achtergrond). Het is misschien onnodig en tijdrovend om daar een volledig design van te maken in photoshop. Onze vraag: kan dit? wat sneller van wireframing naar development gaan?

- .mov: wat meer uitleg?

- scrollanimaties: wat kan er en wat niet?


## Tijdens het gesprek

- design: per pagina moet er een design zijn in PS of AI, en ook de overgangen tussen de verschillende pagina's moeten duidelijk beschreven worden. Overgangen/animaties/veranderingen op één pagina hoeven niet letterlijk gedesigned te worden, een statische weergave is oké.

- .mov: vergeten te vragen

- scrollanimaties: dit kunnen we beter vragen aan Demis of Christof

- het is goed dat we al zoveel research gedaan hebben naar longreads, UX en storytelling


## Na het gesprek

- storytelling: probeer een verhalend geheel te maken, i.p.v. al je onderwerpen op te delen en apart te vertellen

- storytelling: zorg voor een rode draad -> Bowie had verschillende aspecten, neem één hiervan als rode draad

- beginnen met de research naar Bowie zelf

- scrollanimaties: te rade gaan bij Demis of Christof
