# Consult 02
---


## Voor het gesprek

- scrollanimaties: wat kan er en wat niet?


## Tijdens het gesprek

- zorg dat je scrollanimaties in de lijn liggen van wat de mensen verwachten (bv. als je verticaal scrollt dat er dan ook iets verticaals gebeurt)

- het is altijd leuk als er iets extra's gebeurt naast de verwachte animaties

- voorbeeld national geographic: effect bij het bewegen van de muis kan, zal je design niet breken. let hier alleen mee op bij meer zakelijke sites


## Na het gesprek

- zie consult_01.md
