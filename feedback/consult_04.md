# Consult 04
---


## Voor het gesprek

- Zorgen i.v.m. vorig consult: is longread nog mogelijk of moeten we meer naar een devine design?
- Nieuwe ideeën uitgewerkt na vorig consult: kunnen we hier mee verder?


## Tijdens het gesprek

- 'longread meets storytelling' dus longread kan zeker nog
- collage-stijl: mag, met pastelkleuren (voorbeelden laten zien van berlijn), maar ook moderne inbreng


## Na het gesprek

- onze tekst analyseren: quotes uithalen + leuke dingen vertellen om te tonen
- video's analyseren: stijl uithalen
- interactieve delen bepalen: bv. kaartjes, muziekspeler, ...
- visueel werk verzamelen: berlijn, bowie, 'bewegende afbeeldingen' maken uit video's
