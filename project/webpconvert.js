const webp = require(`webp-converter`);
// const fs = require(`fs`);

const convertImages = (folder, images) => {
  images.forEach(image => {
    image = folder + image;
    const dest = image.replace(`.jpg`, `.webp`);
    webp.cwebp(image, dest, `-q 70`, status => {
      console.log(status);
    });
  });
};

const getImages = () => {

  // FOLTER WITH IMAGES
  // const imageFolder = `./src/assets/img/events2/`;
  // fs.readdir(imageFolder, (err, files) => {
  //   convertImages(imageFolder, files);
  // });

  // SINGULAR IMAGE
  const imageFolder = `./src/assets/img/`;
  convertImages(imageFolder, [`bowie-salute.jpg`, `album-covers/heroes.jpg`, `album-covers/lodger.jpg`, `album-covers/low.jpg`]);
};

const init = () => {
  getImages();
};

init();
