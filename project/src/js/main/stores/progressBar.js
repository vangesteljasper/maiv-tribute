import {observable} from 'mobx';

import Bookmark from './Bookmark.js';

class ProgressBar {

  @observable
  progress = 0

  @observable
  bookmarks = []

  pageHeight = 0
  visibleHeight = 0

  scrollPos = 0
  maxScroll = 0

  bookmarkData = [
    {title: `Intro Video`, anchor: `card_1`, position: 0},
    {title: `Thin White Duke Aftermath`, anchor: `card_2`, position: 0},
    {title: `Stumbling Through`, anchor: `null`, position: 0},
    {title: `70 miles`, anchor: `null`, position: 0},
    {title: `Gone Were The Gimmicks`, anchor: `card_3`, position: 0},
    {title: `Leaving L.A.`, anchor: `null`, position: 0},
    {title: `Facing The Audience`, anchor: `null`, position: 0},
    {title: `Emotional Wreckage`, anchor: `null`, position: 0},
    {title: `Where Low Stripped The Paint`, anchor: `card_4`, position: 0},
    {title: `Chanting Through The Structure`, anchor: `null`, position: 0},
    {title: `Layered Carefully`, anchor: `null`, position: 0},
    {title: `Rambling Apology`, anchor: `null`, position: 0},
    {title: `Grandiose Performance`, anchor: `null`, position: 0},
    {title: `The Return Of Bowie`, anchor: `card_5`, position: 0},
    {title: `Homeless Traveller`, anchor: `null`, position: 0},
    {title: `Fearless Ambition`, anchor: `null`, position: 0},
    {title: `Back From The Brink`, anchor: `null`, position: 0},
    {title: `Radio Nostalgie Hitlist`, anchor: `card_6`, position: 0}
  ]

  updateProgress() {
    this.scrollPos = document.body.scrollTop;
    this.progress = this.scrollPos * (100 / this.maxScroll);

    this.checkBookmarks();
  }

  checkBookmarks() {
    this.bookmarks.forEach(bookmark => {
      if (bookmark.position >= this.progress + .1) {
        if (bookmark.passed) bookmark.pass(false);
      } else {
        if (!bookmark.passed) bookmark.pass(true);
      }
    });
  }

  calculateMeasurements() {
    this.pageHeight = Math.max(
      document.body.scrollHeight,
      document.body.offsetHeight,
      document.documentElement.clientHeight,
      document.documentElement.scrollHeight,
      document.documentElement.offsetHeight
    );
    this.visibleHeight = window.innerHeight;
    this.maxScroll = this.pageHeight - this.visibleHeight;
  }

  init() {
    this.calculateMeasurements();
    setInterval(this.calculateMeasurements, 2000);
  }

  parseBookmarks() {
    this.bookmarkData.forEach(bookmark => {
      this.bookmarks.push(
        new Bookmark(bookmark.title, bookmark.anchor, bookmark.position)
      );
    });

    for (let i = 0;i < this.cardBookmarks.length;i ++) {
      let {top} = this.cardBookmarks[i].getBoundingClientRect();
      if (!this.cardBookmarks[i].classList.contains(`card`)) {
        top -= 100;
      }
      const pos = (Math.max(0, top + document.body.scrollTop) / (this.pageHeight - document.documentElement.clientHeight)) * 100;
      this.bookmarks[i].position = pos;
    }

    this.checkBookmarks();
  }

  reloadAfterDeferredStyles() {
    const deferredStyles = document.getElementById(`deferred-styles`);
    if (deferredStyles) {
      deferredStyles.addEventListener(`load`, () => {
        this.calculateMeasurements();
      });
    }
  }

  constructor() {
    this.cardBookmarks = document.getElementsByClassName(`bookmark`);
    this.pageHeight = Math.max(
      document.body.scrollHeight, document.body.offsetHeight, document.documentElement.clientHeight,
      document.documentElement.scrollHeight, document.documentElement.offsetHeight
    );

    this.reloadAfterDeferredStyles();

    document.addEventListener(`scroll`, this.updateProgress.bind(this));
    window.addEventListener(`resize`, () => {
      this.init();
      this.updateProgress();
    });

    this.init();
    this.parseBookmarks();
  }

}

const storeProgressBar = new ProgressBar();

if (process.env.NODE_ENV !== `production`) {
  window.bar = storeProgressBar;
}

export default storeProgressBar;
