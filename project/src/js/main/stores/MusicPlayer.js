import {observable, action} from 'mobx';

class MusicPlayer {

  audio = document.createElement(`audio`)
  volume = 1

  @observable songs = [
    {id: 0, title: `David Bowie - Sound and Vision (Low, 1977)`, file: `assets/audio/soundandvision.mp3`},
    {id: 1, title: `David Bowie - Heroes ("Heroes", 1977)`, file: `assets/audio/heroes.mp3`},
    {id: 2, title: `David Bowie - Look Back in Anger (Lodger, 1979)`, file: `assets/audio/lookbackinanger.mp3`}
  ]
  @observable shown = false
  @observable isPlaying = false
  @observable isMuted = false
  @observable currentSong = 1

  @action play() {
    if (!this.isPlaying) {
      this.audio.play();
      this.isPlaying = true;
    }
  }

  @action reset() {
    this.audio.currentTime = 0;
  }

  @action pause(reset = false, fadeOut = false) {
    if (this.isPlaying) {
      if (fadeOut) {
        this._fadeOut(reset);
      } else {
        this._pause();
        if (reset) this.reset();
      }
    }
  }

  @action fadeOut() {
    this.stop(true);
  }

  @action stop(fadeOut = false) {
    if (!fadeOut) this.pause(true);
    else this.pause(true, true);
  }

  @action setSong(songId) {
    if (this.currentSong !== songId) {
      if (this.isPlaying) {
        this._fadeOut();
        this.changeSongfadeInTimeout = setTimeout(() => {
          this._setSong(songId);
          this.play();
          this._fadeIn();
        }, 1200);
      } else {
        this._setSong(songId);
      }
    }
  }

  @action fadeIn() {
    this.play();
    this._fadeIn();
  }

  _fadeIn() {
    this.fadeInInterval = setInterval(() => {
      if (this.volume < .9) {
        this.volume += .1;
        this.audio.volume = this.volume;
      } else {
        clearInterval(this.fadeInInterval);
        this.volume = 1;
        this.audio.volume = this.volume;
      }
    }, 200);
  }

  _setSong(songId) {
    if (this.changeSongfadeInTimeout) clearTimeout(this.changeSongfadeInTimeout);
    if (songId in this.songs) {
      this.currentSong = this.songs[songId].id;
      this.audio.src = this.songs[songId].file;
    }
  }

  _pause() {
    this.audio.pause();
    this.isPlaying = false;
  }

  _fadeOut(reset) {
    this.fadeOutInterval = setInterval(() => {
      if (this.volume > .1) {
        this.volume -= .1;
        this.audio.volume = this.volume;
      } else {
        clearInterval(this.fadeOutInterval);
        this._pause();
        if (reset) this.reset();
        this.volume = 1;
        this.audio.volume = this.volume;
      }
    }, 100);
  }

  constructor() {
    this.audio.src = this.songs[this.currentSong].file;
  }
}

const storeMusicPlayer = new MusicPlayer();

if (process.env.NODE_ENV !== `production`) {
  window.music = storeMusicPlayer;
}

export default storeMusicPlayer;
