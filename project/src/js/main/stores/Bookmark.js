import {observable, action} from 'mobx';
import uuid from 'uuid';

const Bookmark = class {

  @observable passed = false
  @observable position = 0
  @observable hover = false

  title = ``
  anchor = ``
  key = uuid.v4()

  @action
  pass(status) {
    this.passed = status;
    if (this.passed) {
      this.hoverOn();
      setTimeout(() => {
        this.hoverOff();
      }, 1000);
    }
  }

  @action
  hoverOn() {
    if (this.anchor !== `null`) {
      this.hover = true;
      setTimeout(this.hoverOff, 1000);
    }
  }

  @action
  hoverOff() {
    this.hover = false;
  }

  constructor(title, anchor, position) {
    this.title = title;
    this.anchor = anchor;
    this.position = position;
  }
};

export default Bookmark;
