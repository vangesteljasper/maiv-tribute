import React, {PropTypes} from 'react';

const PlayPauseButton = ({currentAction, actionTogglePlay}) => {

  let svg = (
    <svg className='music-player__controlls__icon' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 500 500'>
      <rect x='124' y='106' width='93' height='289' />
      <rect x='283' y='106' width='93' height='289' />
    </svg>
  );
  if (currentAction === `play`) {
    svg = (
      <svg className='music-player__controlls__icon' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 500 500'>
        <polygon points='359 249 141 394 141 105 359 249 359 249' />
      </svg>
    );
  }

  return (
    <button
      type='button'
      className={`button music-player__controlls__button music-player__controlls__button_${currentAction}`}
      onClick={actionTogglePlay}
    >
      {svg}
    </button>
  );

};

PlayPauseButton.propTypes = {
  currentAction: PropTypes.string.isRequired,
  actionTogglePlay: PropTypes.func.isRequired
};

export default PlayPauseButton;
