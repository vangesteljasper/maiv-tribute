const add = (element, className) => {
  if (!element.classList.contains(className)) {
    element.classList.add(className);
  }
};

const remove = (element, className) => {
  if (element.classList.contains(className)) {
    element.classList.remove(className);
  }
};

export {
  add,
  remove
};
