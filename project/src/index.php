<?php
  define('DS', DIRECTORY_SEPARATOR);
  define('WWW_ROOT', __DIR__ . DS);
  require_once(WWW_ROOT . 'config.php');
?>
<!DOCTYPE html>
<html lang="en" data-css-href="css/main.css">
<head>
  <meta charset="UTF-8">
  <title>David Bowie - Tribute</title>
  <meta name="viewport" content="width=device-width, initial-scale=1"/>
  <meta name="author" content="Van Gestel Nephews">
  <script>
    WebFontConfig = {
      google: {
        families: [
          'Quattrocento:400,700',
          'Open Sans:400,800,800i'
        ]
      },
      custom: {
        families: ['Kabel'],
        urls: ['assets/fonts/kabel/kabel.css']
      }
    };
    (function(d) {
      var wf = d.createElement('script'), s = d.scripts[0];
      wf.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js';
      wf.async = true;
      s.parentNode.insertBefore(wf, s);
    })(document);
  </script>
  <?php echo $critical_css ?>
  <?php echo $css ?>
</head>
<body>
  <h1 class="hidden">A David Bowie Tribute - Berlin Era</h1>
  <div class="wrapper music-player_theme_blue-yellow-white progress-bar_theme_beige menu-button_theme_orange <?php echo $deviceType ?>">

    <button type="button" class="button menu-button">
      <span class="hidden">menu button</span>
      <div class="menu-button__wrapper">
      	<div class="menu-button__icon menu-button__icon_top"></div>
      	<div class="menu-button__icon menu-button__icon_middle"></div>
      	<div class="menu-button__icon menu-button__icon_bottom"></div>
      </div>
    </button>
    <div class="progress-bar"></div>
    <div class="music-player"></div>

    <!-- MENU -->
    <nav class="menu hidden">
      <ol class="menu__list">
        <li class="menu__list__item" data-target-class="card_1">Intro Video</li>
        <li class="menu__list__item" data-target-class="card_2">Thin White Duke Aftermath</li>
        <li class="menu__list__item" data-target-class="card_3">Gone Were The Gimmicks</li>
        <li class="menu__list__item" data-target-class="card_4">Where Low Stripped The Paint...</li>
        <li class="menu__list__item" data-target-class="card_5">The Return Of Bowie</li>
        <li class="menu__list__item" data-target-class="card_6">Radio Nostalgie Hitlist</li>
        <li class="menu__list__item" data-target-class="card_7">This Was Bowie</li>
      </ol>
    </nav>

    <!-- HERO -->
    <section class="card card_0 card_text_black card_title_beige card_bg card_bg_beige">
      <div class="width-limit">
        <header class="card__header">
          <h2 class="card__title">
            <span class="kerning kerning_06">D</span><span class="kerning kerning_2">a</span>vid B<span class="kerning kerning_06">o</span>wie<br>Berlin E<span class="kerning kerning_06">r</span>a
          </h2>
          <p class="card__paragraph">
            A tribute to one of the most refined and interesting<br class="toggle-br">
            artists of all time. Being one of our most featured artists,<br class="toggle-br">
            Bowie is a perfect reason to tune in to Radio Nostalgie.
          </p>
        </header>
      </div>
    </section>

    <!-- VIDEO -->
    <section class="card card_1 card_video card_bg card_bg_black bookmark">
      <video class="card_1__video autoplay-video" src="assets/video/intro.mp4" poster="assets/img/intro-poster.jpg" controls preload></video>
    </section>

    <!-- THIN WHITE DUKE AFTERMATH -->
    <article class="card card_2 card_title_purple card_subtitle_purple card_text_black card_bg card_bg_beige card_mark card_mark_yellow card_text_left card_scroll bookmark">
      <div class="width-limit">
        <div class="card__column">
          <div class="card__text">
            <header class="card__header quote-icon">
              <h2 class="card__title">
                <mark class="mark">Thin</mark>
                <mark class="mark">White</mark>
                <mark class="mark">Duke</mark><br class="toggle-br">
                Aftermath
              </h2>
            </header>
            <h3 class="card__sub-title bookmark">stumbling through</h3>
            <p class="card__paragraph">
              In 1976, David Bowie made the album Station to Station, though he
              claimed not to remember it. “I know it was in L.A., because I’ve read
              it was,” he said in 2004. The album, a brief masterpiece, was recorded
              in a cocaine-fueled haze and was accompanied by a tour that Bowie
              stumbled through, racking up headlines for his alleged pro-fascist
              views. The controversy reached a tipping point right before the tour
              ended, when he pulled into London's Victoria Station in an open-top
              Mercedes convertible and waved to the crowd. A photographer, catching
              him at the end of his wave, alleged that Bowie was giving a Nazi
              salute. In the photo of the incident, you can see Bowie in a jumpsuit
              — thin, with his arm extended. Some of the people in the crowd around
              him appear stunned, some amused, some look away entirely.
            </p>
            <h3 class="card__sub-title bookmark">70 miles</h3>
            <p class="card__paragraph">
              By the time he escaped Los Angeles for Berlin in late 1976, Bowie had
              pushed himself to the brink of mental collapse. The change of scenery
              didn’t exactly result in instant stabilization: Upon his arrival, he
              still stalked around, drunk in clubs, not sleeping. There is a story
              from his early Berlin days about him driving around his hotel’s
              parking garage doing 70 miles per hour, with Iggy Pop in the passenger
              seat, "screaming that he wanted to end it all," until his car ran out
              of gas.
            </p>
          </div>
          <div class="artwork card__artwork ">
            <figure class="artwork__figure">
              <picture class="artwork__image">
                <source class="artwork__image" type="image/webp" srcset="assets/img/bowie-salute.webp">
                <img class="artwork__image" src="assets/img/bowie-salute.jpg" alt="Dabid Bowie Salute">
              </picture>
              <figcaption class="hidden"><span>1976</span> - Vixtoria Station, London</figcaption>
            </figure>
          </div>
        </div>
      </div>
    </article>

    <!-- GONE WERE THE GIMMICKS -->
    <article class="card card_3 card_title_blue card_subtitle_blue card_text_beige card_bg card_bg_orange card_mark card_mark_yellow card_text_right card_scroll bookmark">
      <div class="width-limit">
        <div class="card__column">
          <div class="card__artwork"></div>
          <div class="card__text">
            <header class="card__header quote-icon">
              <h2 class="card__title">Gone Were<br><mark class="mark">The</mark> <mark class="mark">Gimmicks</mark></h2>
            </header>
            <h3 class="card__sub-title bookmark">leaving L.A.</h3>
            <p class="card__paragraph">
              The first album Bowie made after leaving L.A. was Low, released 40
              years ago this year. It's an extension of Station to Station, but more
              reckless, more haunted by the frantic pace of his race away from
              addiction, away from his Thin White Duke persona — the character he
              introduced on Station to Station and played out until it consumed him.
              The move to Berlin was about isolation as much as anything else.
            </p>
            <h3 class="card__sub-title bookmark">facing the audience</h3>
            <p class="card__paragraph">
              Bowie, while kicking a cocaine habit, moved to a city drowning in
              heroin, which he didn’t have a taste for. Berlin was the anti–Los
              Angeles, a place where stardom and celebrity held little currency. The
              work he did there was the work of getting clean and coming clean —
              blowing off the cocaine-dusted mirror and actually looking at himself
              in it, then figuring out a new way to make exciting music that pulled
              only from the well of himself. In this way, getting clean wasn’t just
              about the drugs. Bowie went to Berlin to shed the props and characters
              that had dominated his career until that point. Gone were Ziggy
              Stardust and Halloween Jack. Gone were the gimmicks. Starting with
              Low, Bowie faced his audience and slowly pulled off each mask until
              only the artist beneath them remained.
            </p>
            <h3 class="card__sub-title bookmark">emotional wreckage</h3>
            <p class="card__paragraph">
              The challenge in articulating pain plainly is that the
              artist-to-audience exchange doesn’t benefit as much as when it’s
              dolled up in something pretty. Bowie didn’t dress Low up in anything.
              Despite its occasionally upbeat production, the album is a 40-minute
              ode to emotional wreckage with little hope at the end.
              “Breaking Glass,” the second song on the album but the first with
              lyrics, ends with the lines, “You’re such a wonderful person / But you
              got problems, oh-oh-oh-oh / I'll never touch you.” There’s paranoia
              and restlessness running through the record, splashed with spare and
              experimental moments like “Warszawa,” a six-plus-minute piece of
              largely instrumental music co-driven by Berlin-era collaborator Brian
              Eno. Bowie believed in conveying an emotion through sound more than
              language, through the assumption of a shared feeling. This is the
              trick hiding in Low: a small game of trust. Bowie was asking his
              audience to trust him so that he could maybe trust himself. Clinging
              to our myths is tempting, especially the ones we build for ourselves
              and sit comfortably in for so many years that they become an extension
              of our bodies. Low, more than anything, is the album of Bowie unmaking
              Bowie, in a city where he wasn’t loved enough to get anything but
              clean.
            </p>
          </div>
        </div>
      </div>
    </article>

    <!-- WHERE LOW STRIPPED THE PAINT... -->
    <article class="card card_4 card_title_yellow card_subtitle_yellow card_text_beige card_bg card_bg_grey card_mark card_mark_red card_text_left card_scroll bookmark">
      <div class="width-limit">
        <div class="card__column">
          <div class="card__text">
            <header class="card__header quote-icon">
              <h2 class="card__title">Where Low<br>
                <mark class="mark">Stripped</mark><br>
                <span class="line-height-extra">
                  <mark class="mark">The</mark> <mark class="mark">Paint</mark>
                </span>
              </h2>
            </header>
            <h3 class="card__sub-title bookmark">chanting through<br>the structure</h3>
            <p class="card__paragraph">The album’s title track, one of the brightest flowers in Bowie’s
              bouquet of anthemic masterpieces, is a song that sprawled forward from
              a small and simple moment: Bowie watching Visconti embracing a woman
              outside the studio. Through a single image, Bowie pulled a thread
              through an entire triumphant and visceral story about two lovers
              fighting through the impossibility of borders. Years later, in 1987,
              Bowie played the song in West Berlin at the Wall, near tears, while
              thousands of East Berliners pushed up against it, despite the
              authorities beating them back. The ones who got close enough sang
              through the structure, chanting to the other side as if the song
              itself would crumble the barrier, as if a prayer that powerful could
              bring some small measure of freedom.
            </p>
            <h3 class="card__sub-title bookmark">layered carefully</h3>
            <p class="card__paragraph">
              &quot;Heroes&quot; is Bowie’s great return to his dramatic instincts as a
              storyteller. Where Low stripped the paint off the walls, &quot;Heroes&quot;
              returned to the old paint — not splashed in a brilliant and haphazard
              mess this time, but layered carefully. The music itself is still
              disjointed and clangs deliciously together, so much of it an extension
              of the Bowie/Eno/Visconti sparks that were first set off on Low. But
              in the lyrics, you hear the Bowie who once built entire worlds doing
              it again, but with less fantasy in the mix this time. West Berlin was
              a haunting place to make an album in the 1970s. On one side of the
              Wall there was a no-man’s-land, watched by armed guards with guns and
              instructions to shoot anyone trying to flee. Visconti once said that
              every day they sat down to work, Russian guards were looking at them
              through binoculars, guns over their shoulders. There was no need for
              Bowie to craft a fiction out of this strange violence that seemed
              unreal, which is why &quot;Heroes&quot; sounds more like an artist gently creating
              an archive out of endless burning, complete with small and hopeful
              moments, but always with an eye toward the tragedy that the hope
              needed to be born out of.
            </p>
            <h3 class="card__sub-title bookmark">rambling apology</h3>
            <p class="card__paragraph">
              &quot;Heroes&quot; is said to be Bowie’s most confident album. Low gave him a
              taste of the raw honesty that had been clouded by years of character
              acting and drug-fueled mania, and on &quot;Heroes&quot;, he leaned further into
              those trappings of honest speech, turning the stories in on himself.
              The album, at times, feels like a single stream of consciousness, a
              rush of words about love, alcohol, a crumbling marriage, and money. It
              comes across as powerful because of Bowie’s lack of shame inside that
              territory. “Beauty and the Beast,” which opens the album, is a
              rambling apology for his weaker moments in past years, burying them
              once and for all.
            </p>
            <h3 class="card__sub-title bookmark">grandiose performance</h3>
            <p class="card__paragraph">
              Bowie was 30 years old during the creation of the album, and the
              comfort that comes with surviving three decades and stepping into
              another is present here. Compassion and openness briefly replaced his
              instinct for grandiose performance. He favored nuanced stories,
              ignoring his instinct to bury anything real behind his immeasurable
              taste for cleverness. By the album’s end, as the song “The Secret Life
              of Arabia” — with its danceable highs and lows complemented by Bowie’s
              vocal acrobatics — fades out, it feels like the artist had truly
              accomplished something, or at least found a new way of seeing. He was
              seeing not just himself, but the world around him that was, in many
              ways, as terrifying as it had always been.
            </p>
          </div>
          <div class="card__artwork">
            <div class="card__artwork__background"></div>
            <div class="card__artwork__carousel"></div>
          </div>
        </div>
      </div>
    </article>

    <!-- RETURN OF BOWIE -->
    <section class="card card_5 card_title_blue card_subtitle_blue card_text_beige card_bg card_bg_red card_mark card_mark_yellow card_text_right card_scroll bookmark">
      <div class="width-limit">
        <div class="card__column">
          <div class="card__artwork">
            <video class="card__artwork__video autoplay-video" src="assets/video/thereturnofbowie.mp4" loop>
            </video>
          </div>
          <div class="card__text">
            <header class="card__header quote-icon">
              <h2 class="card__title"><mark class="mark">The</mark> <mark class="mark">Return</mark> Of Bowie</h2>
            </header>
            <h3 class="card__sub-title bookmark">homeless traveller</h3>
            <p class="card__paragraph">
              Lodger, the last album in the Berlin Trilogy, blended what Bowie had
              remembered he was capable of on his previous two efforts. After spending
              1978 touring Low and &quot;Heroes&quot;, he returned to work, dreaming up another
              concept, this one about a homeless traveler. Lodger is a part of the
              Berlin Trilogy in name and collaborative spirit only.
            </p>
            <h3 class="card__sub-title bookmark">fearless ambition</h3>
            <p class="card__paragraph">
              While Bowie once again teamed up with Eno and Visconti, no part of the
              album was recorded in Berlin; by then Bowie was splitting his time
              between Switzerland and New York. The album sounds exhausted, like the
              collaborative spark was beginning to peter out. It takes large swings,
              attempting to tie together a travelogue with a critique of culture. It’s
              a good album, one that has aged well, but it doesn’t hold up to the
              sharpness of its siblings. The victory in Lodger is in its ambition —
              the return of Bowie, as fearless as many remembered him, but no longer
              dragged down by his vices. There is a clarity in his fierce attempts and
              a comfort in trying that which might surely fail.
            </p>
            <h3 class="card__sub-title bookmark">back from the brink</h3>
            <p class="card__paragraph">
              The truth, though, is that Lodger only matters as a close to a chapter.
              By the time it arrived, Bowie’s work was done. He had already pulled
              himself back from the brink, and done it while providing theater to the
              masses in a city where he was often invisible, just another face in a
              crowd. Making it through hell and back means nothing if the interior of
              suffering doesn’t have a language that you can, perhaps, tie to a
              feeling that maybe pulls someone out of that corner of hell with you.
              The Berlin albums, particularly the first two, are unselfish, relentless
              in the pursuit of self-examination and the mourning of failures.
            </p>
          </div>
        </div>
      </div>
    </section>

    <!-- RADIO NOSTALGIE HITLIST -->
    <section class="card card_6 card_title_blue card_subtitle_black card_text_black card_bg card_bg_beige card_text_left card_mark card_mark_yellow card_text_left bookmark">
      <div class="width-limit">
        <div class="card__column">
          <div class="card__artwork berlin-trilogy"></div>
          <div class="card__text">
            <header class="card__header">
              <h2 class="card__title">
                <mark class="mark">Radio</mark>
                <mark class="mark">Nostalgie</mark><br>
                Hitlist
              </h2>
            </header>
            <p class="card__paragraph">
              The albums Low, &quot;Heroes&quot; as well as Lodger, all from Bowie’s
              Berlin era, were in Radio Nostalgie’s Album Top 100, with &quot;Heroes&quot;
              ranking the highest at eight place.
              Listen To Them now!
            </p>
          </div>
        </div>
      </div>
    </section>

    <!-- THIS WAS BOWIE -->
    <section class="card card_7 card_title_beige card_subtitle_beige card_text_beige card_bg card_bg_purple card_text_left card_mark card_mark_orange">
      <div class="width-limit">
        <div class="card__text">
          <header class="card__header quote-icon">
            <h2 class="card__title">This Was <mark class="mark">Bowie</mark></h2>
          </header>
          <div class="card__text__column">
            <p class="card__paragraph">
              Bowie’s legacy seems to rest on his uncanny ability to invite
              listeners into a conversation. For all of his theatrics and props, he
              managed to always offer a front-row seat to the dance, even if that
              dance was his life shaking itself down and being slowly pieced back
              together. Of course, Bowie did not leave us as a near-30-year-old
              speeding around in a car with Iggy Pop. He left us the way he lived
              with us: providing small and intimate gifts of his interior life,
              wrapping them in something so spectacular that we might, briefly,
              forget why we’d arrived in the first place.
            </p>
            <p class="card__paragraph">
              Last year, when the doctors told him there was nothing else they could
              do, when he knew what the world was still unaware of, Bowie writhed on
              his deathbed in the video for “Lazarus” and sang, “I’ve got scars that
              can’t be seen.” This was Bowie, at the end of his life, still telling
              us exactly what we needed to know, even before we knew that we needed
              to know it.<br><br>
              He never let go of what he learned when the stakes were highest, in a
              city trying to rebuild itself at all costs.
            </p>
          </div>
        </div>
      </div>
    </section>
  </div>
  <?php echo $js ?>
</body>
</html>
