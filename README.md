# Major IV Tribute

Major Assignment


first, change directory to /project.

`$ cd /project`

Then download the dependencies.

`$ yarn` or `$ npm install`

## development

**start development server WITH mamp**

`$ yarn run dev`

**start development server WITHOUT mamp**

`$ yarn run dev_`

**start development server in dashboard WITH mamp**

`$ yarn run dashboard`

**start development server in dashboard WITHOUT mamp**

`$ yarn run dashboard_`

## production

**compile production build**

`$ yarn run build`

**serve production build with mamp**

`$ yarn run serve`
